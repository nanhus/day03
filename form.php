<?php
	$gender = array(0 => 'Nam',1 => 'Nữ');
	$faculty = array('Khoa học máy tính' => 'MAT','Khoa học vật liệu' => 'KDL');
	echo
    "
		<head>
            <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
			<link rel='stylesheet' type='text/css'  href='form.css'>
		</head>
		
		<body>
            <div>
                <form>
                    <div>
                        <div id='input'>
                            <label id='label'>Họ và tên</label>
                            <input type='text' name='username'/>
                        </div>
                        
                        <div id='input'>
                            <label id='label'>Giới tính</label>";
                            for($i=0;$i<=1;$i++){
                                echo
                                "<input type='radio' name='gender'> <label id='gender'>".$gender[$i]."</label>";
                            }

                        echo
                        "</div>
                        <div id='input'>
                            <label id='label'>Phân khoa</label>
                            <select name='faculty'>
                                <option></option>";
                                foreach($faculty as $item => $label){
                                    echo "<option>".$item."</option>";
                                }
                        echo
                        "</select>
                        </div>
                    </div>

                    <div>
                        <button>Đăng ký</button>
                    </div>

                </form>
            </div>
		</body>
	"
?>